import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class CFGraph {
	
	private List<CFNode> allNodes;
	private List<Register> allRegisters;
	//everytime we need a new graph is because we have a new function.
	private String functionName;
	
	public CFGraph(String functionName){
		this.allNodes = new ArrayList<CFNode>();
		this.allRegisters = new ArrayList<Register>();
		this.functionName = functionName;
	}
	
	public void add(CFNode newNode){
		allNodes.add(newNode);
	}
	
	public List<CFNode> getAllNodes(){
		return allNodes;
	}
	
	public void addRegister(int ID){
		allRegisters.add(new Register(ID));
	}
	
	public List<Register> getAllRegister(){
		return allRegisters;
	}
	
	public Register findOrAddRegister(int ID){
		for (Register r:allRegisters){
			if (r.getID() == ID){
				return r;
			}
		}
		Register r = new Register(ID);
		allRegisters.add(r);
		return r;
	}
	
	public CFNode getNode(int index){
		return allNodes.get(index);
	}
	
	public void setStart(CFNode newStart){
		if (allNodes.contains(newStart)){
			allNodes.remove(newStart);
		}
		allNodes.add(0, newStart);
	}
	
	public void setEnd(CFNode newStart){
		if (allNodes.contains(newStart)){
			allNodes.remove(newStart);
		}
		allNodes.add(newStart);
	}
	
	public CFNode getStart(){
		return allNodes.get(0);
	}
	
	public CFNode getLast(){
		return allNodes.get(allNodes.size()-1);
	}
	
	public CFNode getByID(int id){
		for (CFNode n:this.allNodes){
			if (n.getId()==id){
				return n;
			}
		}
		return null;
	}
	
	public void removeUnreachableNode(){
		//identify unreachable node and put a mark on reachable node using DFS
		//, the second algorithm on wikipedia.
		Stack<CFNode> S = new Stack<CFNode>();
		S.push(this.allNodes.get(0));
		while (!S.isEmpty()){
			CFNode v = S.pop();
			if (!v.isReached()){
				v.reach();
				if (v.getLeftChild()!=null){
					S.push(v.getLeftChild());
				}
				if (v.getRightChild()!=null){
					S.push(v.getRightChild());
				}
			}
		}
		int index = 0;
		while (index<allNodes.size()){
			if (!allNodes.get(index).isReached()){
				allNodes.remove(index);
			}else{
				index++;
			}
		}
	}
	
	
	
	@Override
	public String toString(){
		String toReturn = "(" + this.functionName + '\n';
		for (CFNode n : allNodes){
			toReturn += n.toString();
		}
		toReturn += ")";
		return toReturn;
	}
}
