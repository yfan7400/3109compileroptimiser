import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompilerOptimizer {
	
	public List<CFNode> allNodes = new ArrayList<CFNode>();
	public static List<String> fileLines = new ArrayList<String>();
	public static String unoptimizedFileName = "";
	public static String targetFileName = "";
	
	public static List<CFGraph> graphList;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length == 0){
			System.out.println("Not Completed, the available options of optimization.");
		}else if(args.length>=2){
			unoptimizedFileName = args[0];
			targetFileName = args[1];
		}
		CFNode currentNode = new CFNode(00);  // may adjust this later on

        //List<CFNode> theFunction = new ArrayList<CFNode>(); // a function is just a list of Nodes/ Blocks
		//One graph for a function as defined on piazza. list of graph(functions) for a program.
        graphList = new ArrayList<CFGraph>();
        //new ArrayList<Instruction>();

        //File file = new File("/path/to/my/file.txt");
        //  /Users/SoL/3109compileroptimiser/readTest.txt

        //File file = new File("/Users/SoL/3109compileroptimiser/readTest.txt");

        // File file = new File("/Users/SoL/3109compileroptimiser/readTest.txt");
        try
        {
            System.out.println("What the file contained: ");

            // Open the file that is the first
            // command line parameter

            // using MAC here,  the directory  format might be different depneindg on OS,
            // also the libraries that are required to import


            // simpleMain.txt,  instructionsList.txt,  multiBlocks.txt
            FileInputStream fstream = new FileInputStream(unoptimizedFileName);   //   simpleMain.txt


            // Get the object of DataInputStream
            DataInputStream inputStream = new DataInputStream(fstream);

            BufferedReader brReader = new BufferedReader(new InputStreamReader(inputStream));
            String strLine;

            //Read File Line By Line
            while ((strLine = brReader.readLine()) != null)
            {

                String editedString = editLine(strLine); // removing all brackets and  spacing front/ end

                //System.out.println(editedString);   // use this to see effect
                System.out.println(strLine);

                String[] splitEditedArray = editedString.split(" ");

                //if(splitEditedArray[0].equals("0"))
                //("[0-9].*")
                if(splitEditedArray[0].matches("[0-9].*")) // if the first one is a (block) number,   0, 19, 100 all work
                {

//                    System.out.println("You got a new block");
                    
                    //construct

                    int theId = Integer.parseInt(splitEditedArray[0]);
                    currentNode = new CFNode(theId);

                    //myNode.showContent();

                    editedString = "";

                    //  basically just remove the first element (  101 ld r1 n )  ----->  ld r1 n
                    for (int i = 1; i < splitEditedArray.length; i++)
                    {
                        editedString+= splitEditedArray[i] + " ";
                    }

                    editedString = editedString.trim();
                    splitEditedArray = editedString.split(" ");

                    //editedString = splitEditedArray.substring(1);
                }

                if(splitEditedArray[0].equals("ld")) //ld r3 tmp
                {
//                    System.out.println("You got an ld");

                    //String justTheInt = splitEditedArray[1].replaceAll("r", "");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));
                    currentNode.addInstruction(new Load(graphList.get(graphList.size()-1).findOrAddRegister(theRegister), splitEditedArray[2]));

                }


                else if(splitEditedArray[0].equals("lc"))
                {
                    //System.out.println("You got a lc");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    int constant = Integer.parseInt(splitEditedArray[2]);

                    currentNode.addInstruction(new LoadConstant(graphList.get(graphList.size()-1).findOrAddRegister(theRegister), constant));

                }

                else if(splitEditedArray[0].equals("st")) //st tmp r2
                {
                    //System.out.println("You got a st " + splitEditedArray[1].toString());

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[2].replaceAll("r", ""));

                    currentNode.addInstruction(new StoreInst(graphList.get(graphList.size()-1).findOrAddRegister(theRegister), splitEditedArray[1].toString()));

                }

                // Arithmatic

                else if(isArithmetic(splitEditedArray[0])) // add r3 r4 r5
                {
                    //System.out.println("You got a Math");

                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    int registerOp1 = Integer.parseInt(splitEditedArray[2].replaceAll("r", ""));
                    int registerOp2 = Integer.parseInt(splitEditedArray[3].replaceAll("r", ""));

                    //public Arithmetic(String type, int regT, int regOp1, int regOp2){
                    Register Reg = graphList.get(graphList.size()-1).findOrAddRegister(theRegister);
                    Register Reg1 = graphList.get(graphList.size()-1).findOrAddRegister(registerOp1);
                    Register Reg2 = graphList.get(graphList.size()-1).findOrAddRegister(registerOp2);
                    currentNode.addInstruction(new Arithmetic(splitEditedArray[0], Reg, Reg1, Reg2));
                }
                // Comparison

                else if(isComparison(splitEditedArray[0]))   // eq r3 r4 r5 ,   lt r1 r2 r4
                {
                    //System.out.println("You got a Comparison");

                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    int registerOp1 = Integer.parseInt(splitEditedArray[2].replaceAll("r", ""));
                    int registerOp2 = Integer.parseInt(splitEditedArray[3].replaceAll("r", ""));

                    // public Comparison(String type, int regT, int regOp1, int regOp2){
                    Register Reg = graphList.get(graphList.size()-1).findOrAddRegister(theRegister);
                    Register Reg1 = graphList.get(graphList.size()-1).findOrAddRegister(registerOp1);
                    Register Reg2 = graphList.get(graphList.size()-1).findOrAddRegister(registerOp2);
                    currentNode.addInstruction(new Comparison(splitEditedArray[0], Reg, Reg1, Reg2));
                }

                else if(splitEditedArray[0].equals("call")) //st tmp r2
                {
                    //System.out.println("You got a call");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    List<Register> registerList = new ArrayList<Register>();
                    
                    // private List<Integer> registerParameterList;
                    for(int i = 3; i < splitEditedArray.length; i++) // storing all registers input
                    {
                        int theInputRegister = Integer.parseInt(splitEditedArray[i].replaceAll("r", ""));
                        Register InputRegister =  graphList.get(graphList.size()-1).findOrAddRegister(theInputRegister);
                        registerList.add(InputRegister);
                    }

                    //CallInst(int registerReceiver, String functionName, List<Integer> registerParameterList)
                    currentNode.addInstruction(new CallInst(graphList.get(graphList.size()-1).findOrAddRegister(theRegister), splitEditedArray[2], registerList));

                }

                //current assume br / ret are the only two possible last instruction,
                else if(splitEditedArray[0].equals("ret")) //ret r3
                {
                    //System.out.println("You got a ret");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    currentNode.addInstruction(new ReturnInst(graphList.get(graphList.size()-1).findOrAddRegister(theRegister)));

                    //current assume br / ret are the only two possible last instruction, then add into function
                    //??I doubt it
                    graphList.get(graphList.size()-1).add(currentNode);

                    //System.out.println("Block number: " + theFunction.size());

                }

                else if(splitEditedArray[0].equals("br"))
                {
                    //System.out.println("You got a branch");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    int leftBranch = Integer.parseInt(splitEditedArray[2]);
                    int rightBrnach = Integer.parseInt(splitEditedArray[3]);

                    currentNode.addInstruction(new Branch(graphList.get(graphList.size()-1).findOrAddRegister(theRegister), leftBranch, rightBrnach));

                    //current assume br / ret are the only two possible last instruction, then add into function
                    //??
                    graphList.get(graphList.size()-1).add(currentNode);

                    //System.out.println("Block number: " + theFunction.size());
                }
                else {
//                	System.out.println("You got a function!");
                	//construct a new graph here.
                	graphList.add(new CFGraph(editedString));
                }
            }
            //Close the input stream
            inputStream.close();
//            System.out.println("Number of blocks stored: " + theFunction.size());
            
            establishEdges();
            
            for (CFGraph g:graphList){
            	g.removeUnreachableNode();
            }
            
            computeUsedReg();
            
//            System.out.println("Printing stored blocks before: " );
//            printProgram();
            
            eliminateDeadCode();
            
            System.out.println("Printing stored blocks after: " );
            printProgram();
        }

        catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }


//        for (int i = 0; i < theFunction.size(); i++)
//        {
//            theFunction.get(i).showContent();
//
//        }

    // end of Main
    }

		
		//read file
		
		
		
	
	
	public static void printProgram(){
		PrintWriter writer;
		try {
			writer = new PrintWriter(targetFileName, "UTF-8");
			for (CFGraph g:graphList){
				writer.println('(' + g.toString() + ')');
	    		System.out.print('(' + g.toString() + ')');
	    	}
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	  
    }
    
    
    
    private static String editLine(String uneditedLine)
    {
        //String editedLine

        String editedString = uneditedLine.replaceAll("\\(", ""); // removing all ( each line
        editedString = editedString.replaceAll("\\)", "");   // removing all )  each line

        editedString = editedString.trim();   // removing all spacing front / end

        return editedString;
    }

    private static boolean isArithmetic(String inputString)
    {
        //ArrayList<String> listOfArith = new ArrayList<String>();
        //listOfArith = ["add", "sub", "mul", "div"];

        String[] listOfArith = new String[] {"add","sub","mul","div"};

        if(Arrays.asList(listOfArith).contains(inputString))
            return true;
        //System.out.println("well you got " + inputString + " here");

        else
            return false;
    }


    private static boolean isComparison(String inputString)
    {

        String[] listOfComparison = new String[] {"lt","gt","eq"};

        if(Arrays.asList(listOfComparison).contains(inputString))
            return true;

        else
            return false;

    }
    
    private static void eliminateDeadCode(){
    	
    	for(CFGraph g: graphList){
    		List<Register> allRegisters = g.getAllRegister();
    		for(CFNode n:g.getAllNodes()){
    			boolean[] usedReg = new boolean[g.getAllRegister().size()];
    			for (int i=0;i<usedReg.length;i++){
    				if (n.getLeftChild()!=null){
    					usedReg[i] = n.getLeftChild().usedReg[i] || n.getRightChild().usedReg[i];
    				}
    				else{
    					usedReg[i] = false;
    				}
    			}
    			for (int j = n.getInstructionList().size()-1;j >=0; j--){
    				Instruction i = n.getInstructionList().get(j);
    				if (i.getType().equals("ReturnInst")){
    					ReturnInst temp = (ReturnInst)i;
    					int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
    					if (index != -1){//which shouldn't be
    						usedReg[index]=true;
    					}else{System.out.println("error:register not found");}
    					n.getInstructionList().get(j).used=true;
    				}else if(i.getType().equals("StoreInst")){
    					StoreInst temp = (StoreInst)i;
    					int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
    					if (index != -1){//which shouldn't be
    						usedReg[index]=true;
    					}else{System.out.println("error:register not found");}
    					n.getInstructionList().get(j).used=true;
    				}else if(i.getType().equals("Branch")){
    					Branch temp = (Branch)i;
    					int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
    					if (index != -1){//which shouldn't be
    						usedReg[index]=true;
    					}else{System.out.println("error:register not found");}
    					n.getInstructionList().get(j).used=true;
    				}else if(i.getType().equals("CallInst")){
    					CallInst temp = (CallInst)i;
    					int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
    					if (index == -1){System.out.println("error:register not found");}
    					if (usedReg[index]){
    						n.getInstructionList().get(j).used=true;
    						List<Register> paraList = temp.getRegisterOp();
    						for (Register r:paraList){
    							index = getIndexofRegister(allRegisters,r.getID());
    							if (index != -1){//which shouldn't be
    								usedReg[index]=true;
    							}else{
    								System.out.println("error:register not found");
    							}
    						}	
    					}
    				}else if((i.getType().equalsIgnoreCase("add"))||(i.getType().equalsIgnoreCase("sub"))||(i.getType().equalsIgnoreCase("mul"))||(i.getType().equalsIgnoreCase("div"))){
    					Arithmetic temp = (Arithmetic)i;
    					int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
    					if (index == -1){System.out.println("error:register not found");}
    					if (usedReg[index]){
    						n.getInstructionList().get(j).used=true;
    						index = getIndexofRegister(allRegisters,temp.getRegisterOp1().getID());
    						if (index != -1){//which shouldn't be
    							usedReg[index]=true;
    						}else{
    							System.out.println("error:register not found");
    						}
    						index = getIndexofRegister(allRegisters,temp.getRegisterOp2().getID());
    						if (index != -1){//which shouldn't be
    							usedReg[index]=true;
    						}else{
    							System.out.println("error:register not found");
    						}	
    					}
    				}else if((i.getType().equalsIgnoreCase("lt"))||(i.getType().equalsIgnoreCase("gt"))||(i.getType().equalsIgnoreCase("eq"))){
    					Comparison temp = (Comparison)i;
    					int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
    					if (index == -1){System.out.println("error:register not found");}
    					if (usedReg[index]){
    						n.getInstructionList().get(j).used=true;
    						index = getIndexofRegister(allRegisters,temp.getRegisterOp1().getID());
    						if (index != -1){//which shouldn't be
    							usedReg[index]=true;
    						}else{
    							System.out.println("error:register not found");
    						}
    						index = getIndexofRegister(allRegisters,temp.getRegisterOp2().getID());
    						if (index != -1){//which shouldn't be
    							usedReg[index]=true;
    						}else{
    							System.out.println("error:register not found");
    						}	
    					}
    				}else if(i.getType().equals("Load")){
    					Load temp = (Load)i;
//    					if (n.getId()==0){
//    						System.out.println("_____________________________r1.used=" +usedReg[getIndexofRegister(allRegisters,0)]);
//    					}
    					int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
    					if (index != -1){//which shouldn't be
    						if (usedReg[index]==true){
    							n.getInstructionList().get(j).used=true;
    						}
    						
    					}else{System.out.println("error:register not found");}
//    					n.getInstructionList().get(j).used=true;
    				}
    				else if(i.getType().equals("LoadConstant")){
    					LoadConstant temp = (LoadConstant)i;
    					int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
    					if (index != -1){//which shouldn't be
    						if (usedReg[index]==true){
    							n.getInstructionList().get(j).used=true;
    						}
    						
    					}else{System.out.println("error:register not found");}
//    					n.getInstructionList().get(j).used=true;
    				}
    				
    			}
//    			System.out.println("Printing stored blocks before: " );
//                printProgram();
//   			 System.out.println("removing" );
   			for (int j = n.getInstructionList().size()-1;j >=0; j--){
   				Instruction i = n.getInstructionList().get(j);
					if (i.used==false){
						n.getInstructionList().remove(i);
						j = n.getInstructionList().size()-1;
					}
					
				}
//   			System.out.println("finished removing" );
    		}
    	}
    }
    
    public static int getIndexofRegister(List<Register> allRegisters, int ID){
		for (int i=0;i<allRegisters.size();i++){
			if (allRegisters.get(i).getID() == ID){
				return i;
			}
		}
		return -1;
	}
    
    private static void computeUsedReg(){
    	for(CFGraph g: graphList){
    		for(CFNode n:g.getAllNodes()){
    			if (n.usedReg==null){
    				n.computeUsedReg(g.getAllRegister());
    			}
    		}
    	}
    }
    
    private static void establishEdges(){
    	for (CFGraph singleGraph : graphList){
    		for (CFNode singleNode : singleGraph.getAllNodes()){
    			if (singleNode.getLastInstruction().getType().equals("Branch")){
    				Branch lastBranch = (Branch) singleNode.getLastInstruction();
    				
//    				System.out.println(lastBranch.toString());
    				CFNode tempNode = singleGraph.getByID(lastBranch.getLeft());
    				if (tempNode!=null){
    					singleNode.setLeftChild(tempNode);
    				}
    				else{
    					System.out.println(lastBranch.toString() + "  didn't find left block" );
    				}
    				
    				tempNode = singleGraph.getByID(lastBranch.getRight());
    				if (tempNode!=null){
    					singleNode.setRightChild(tempNode);
    				}
    				else{
    					System.out.println(lastBranch.toString() + "  didn't find right block" );
    				}
    			}
    		}
    	}
    }
	
	
//	public static void ReadFile(){
//		try
//        {
//            // Open the file that is the first
//            // command line parameter
//
//            // using MAC here,  the directory  format might be different depneindg on OS,
//            // also the libraries that are required to import
//            FileInputStream fstream = new FileInputStream("readTest.txt");
//
//            // Get the object of DataInputStream
//            DataInputStream inputStream = new DataInputStream(fstream);
//
//            BufferedReader brReader = new BufferedReader(new InputStreamReader(inputStream));
//            String strLine;
//
//            //Read File Line By Line
//            while ((strLine = brReader.readLine()) != null)
//            {
//                // Print the content on the console
//                fileLines.add(strLine);
//            }
//            //Close the input stream
//            inputStream.close();
//        }
//
//        catch (Exception e){//Catch exception if any
//            System.err.println("Error: " + e.getMessage());
//        }
//	}

}
