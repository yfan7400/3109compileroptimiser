import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * @author yuan
 *
 */
public class CFNode {  // Node is same as  a Code Block
	private List<CFNode> parents;
	private CFNode leftChild;
	private CFNode rightChild;
	private List<Instruction> instructionList;
	private boolean reached;
	public boolean[] usedReg;
	
	private int blockId;

	// i think setting it into 2 children might be better
	//private CFNode firstChild;
	//private CFNode secondChild;

	public CFNode(int id) {
//		this.leftChilds = new ArrayList<CFNode>();
		this.parents = new ArrayList<CFNode>();
		this.instructionList = new ArrayList<Instruction>();
		this.reached = false;

		this.blockId = id;

		this.leftChild = null;
		this.rightChild = null;
	}

	public void reach(){
		this.reached = true;
	}
	
	public boolean isReached(){
		return reached;
	}
	
	public void setLeftChild(CFNode child) {
		this.leftChild = child;
		child.parents.add(this);
	}
	
	public void setRightChild(CFNode child) {
		this.rightChild = child;
		child.parents.add(this);
	}
//	public void addParent(CFNode parent){
//		this.parents.add(parent);
//	}

	public int getId(){
		return blockId;
	}
	
	public CFNode getLeftChild() {
		return leftChild;
	}
	
	public CFNode getRightChild() {
		return rightChild;
	}
	
	public List<CFNode> getParent() {
		return parents;
	}
	
	public void addInstruction(Instruction newInstruction) {
		instructionList.add(newInstruction);
	}

	public void removeInstruction(int Index) {
		instructionList.remove(Index);
	}

	public List<Instruction> getInstructionList() {
		return instructionList;
	}

	public void showContent()
	{
		System.out.print("(" + this.blockId + " ");

		for(int i = 0; i < this.instructionList.size(); i++)
		{
			//String hi = this.instructionList.get(i).toString();

			if(i != this.instructionList.size() - 1)
				System.out.print("("+ this.instructionList.get(i).toString() + ")\n");
			else
				System.out.print("("+ this.instructionList.get(i).toString() + ")");
		}

		System.out.print(") \n");
	}
	
	public Instruction getLastInstruction(){
		return this.instructionList.get(this.instructionList.size()-1);
	}
	
	@Override
	public String toString(){
		String toReturn = "(" + this.blockId;
		for (Instruction i:this.instructionList){
			toReturn += " (";
			toReturn += i.toString();
			toReturn += ')';
			toReturn += '\n';
		}
		toReturn += ")";
//		toReturn += ("         isReached?" + this.reached);
		toReturn += '\n';
		return toReturn;
	}
	
	public void computeUsedReg(List<Register> allRegisters){
		this.usedReg= new boolean[allRegisters.size()];
		if (leftChild!=null){
			if (leftChild.usedReg==null){
				leftChild.computeUsedReg(allRegisters);
			}
			if (rightChild.usedReg==null){
				rightChild.computeUsedReg(allRegisters);
			}
			boolean leftReg[] = this.leftChild.usedReg;
			boolean rightReg[] = this.rightChild.usedReg;
			for (int i=0;i<leftReg.length;i++){
				usedReg[i]=leftReg[i]||rightReg[i];
			}
		}else{
			for (int i=0;i<allRegisters.size();i++){
				usedReg[i]=false;
			}
		}
		for (int j=instructionList.size()-1;j>=0;j--){ 
			Instruction i=instructionList.get(j);
			if (i.getType().equals("ReturnInst")){
				ReturnInst temp = (ReturnInst)i;
				int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
				if (index != -1){//which shouldn't be
					usedReg[index]=true;
				}else{System.out.println("error:register not found");}
			}else if(i.getType().equals("StoreInst")){
				StoreInst temp = (StoreInst)i;
				int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
				if (index != -1){//which shouldn't be
					usedReg[index]=true;
				}else{System.out.println("error:register not found");}
			}else if(i.getType().equals("Branch")){
				Branch temp = (Branch)i;
				int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
				if (index != -1){//which shouldn't be
					usedReg[index]=true;
				}else{System.out.println("error:register not found");}
			}else if(i.getType().equals("CallInst")){
				CallInst temp = (CallInst)i;
				int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
				if (index == -1){System.out.println("error:register not found");}
				if (usedReg[index]){
					List<Register> paraList = temp.getRegisterOp();
					for (Register r:paraList){
						index = getIndexofRegister(allRegisters,r.getID());
						if (index != -1){//which shouldn't be
							usedReg[index]=true;
						}else{
							System.out.println("error:register not found");
						}
					}	
				}
			}else if((i.getType().equalsIgnoreCase("add"))||(i.getType().equalsIgnoreCase("sub"))||(i.getType().equalsIgnoreCase("mul"))||(i.getType().equalsIgnoreCase("div"))){
				Arithmetic temp = (Arithmetic)i;
				int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
				if (index == -1){System.out.println("error:register not found");}
				if (usedReg[index]){
					index = getIndexofRegister(allRegisters,temp.getRegisterOp1().getID());
					if (index != -1){//which shouldn't be
						usedReg[index]=true;
					}else{
						System.out.println("error:register not found");
					}
					index = getIndexofRegister(allRegisters,temp.getRegisterOp2().getID());
					if (index != -1){//which shouldn't be
						usedReg[index]=true;
					}else{
						System.out.println("error:register not found");
					}	
				}
			}else if((i.getType().equalsIgnoreCase("lt"))||(i.getType().equalsIgnoreCase("gt"))||(i.getType().equalsIgnoreCase("eq"))){
				Comparison temp = (Comparison)i;
				int index = getIndexofRegister(allRegisters,temp.getRegister().getID());
				if (index == -1){System.out.println("error:register not found");}
				if (usedReg[index]){
					index = getIndexofRegister(allRegisters,temp.getRegisterOp1().getID());
					if (index != -1){//which shouldn't be
						usedReg[index]=true;
					}else{
						System.out.println("error:register not found");
					}
					index = getIndexofRegister(allRegisters,temp.getRegisterOp2().getID());
					if (index != -1){//which shouldn't be
						usedReg[index]=true;
					}else{
						System.out.println("error:register not found");
					}	
				}
			}
		}
		
		
	}
	
//	public boolean[] getUsedReg(){
//		return usedReg;
//	}
	
	public int getIndexofRegister(List<Register> allRegisters, int ID){
		for (int i=0;i<allRegisters.size();i++){
			if (allRegisters.get(i).getID() == ID){
				return i;
			}
		}
		return -1;
	}
	
}


/*
	public List<CFNode> getChildSecondVersion()
	{
		List<CFNode> output = new ArrayList<CFNode>();
		output.add(this.firstChild);
		output.add(this.secondChild);

		return output;
	}

	public void setFirstChild(CFNode child)
	{
		this.firstChild = child;
	}

	public void setSecondChild(CFNode child)
	{
		this.secondChild = child;
	}
*/