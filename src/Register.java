/**
 * 
 */

/**
 * @author yuan
 *
 */
public class Register {
	
	private int ID;
	private int value;
	private boolean used;
	
	public Register(int ID){
		this.ID = ID;
		this.value = 0;
		this.used = false;
	}
	
	public void setUsed(){
		this.used = true;
	}
	
	public boolean isUsed(){
		return used;
	}
	
	public int getID(){
		return ID;
	}
	
	public int getValue(){
		return value;
	}
	
	public void setValue(int value){
		this.value = value;
	}
	
	@Override
	public String toString(){
		return "r"+ID;
	}
}
