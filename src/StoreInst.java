/**
 * 
 */

/**
 * @author yuan
 *
 */
public class StoreInst extends Instruction {

	private Register register;
	private String variable;
	
	public StoreInst(Register register, String variable){
		this.register = register;
		this.variable = variable;
		this.used = false;
	}
	
	/* (non-Javadoc)
	 * @see Instruction#toString()
	 */
	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return "st " + this.variable + " " + this.register;
	}

	/* (non-Javadoc)
	 * @see Instruction#getType()
	 */
	@Override
	public String getType()
	{
		// TODO Auto-generated method stub
		return "StoreInst";
	}
	
	public Register getRegister(){
		return register;
	}

}
