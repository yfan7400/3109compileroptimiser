/**
 * 
 */

/**
 * @author yuan
 * Interface of Instructions.
 */
public abstract class Instruction {
	public boolean used;
	public abstract String toString();
	public abstract String getType();
}
