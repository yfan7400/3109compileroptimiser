/**
 * 
 */

/**
 * @author yuan
 *
 */
public class ReturnInst extends Instruction {

	private Register register;
	
	public ReturnInst(Register register)
	{
		this.register = register;
		this.used = false;
	}
	
	/* (non-Javadoc)
	 * @see Instruction#toString()
	 */
	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return "ret " + this.register + " " + used;
	}

	/* (non-Javadoc)
	 * @see Instruction#getType()
	 */
	@Override
	public String getType()
	{
		// TODO Auto-generated method stub
		return "ReturnInst";
	}
	
	public Register getRegister(){
		return register;
	}

}
