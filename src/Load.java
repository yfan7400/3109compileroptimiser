/**
 * 
 */

/**
 * @author yuan
 *
 */
public class Load extends Instruction {

	private Register register;
	private String variable;
	
	public Load (Register register,String variable){
		this.register = register;
		this.variable = variable;
		this.used = false;
	}
	public Register getRegister(){
		return register;
	}
	/* (non-Javadoc)
	 * @see Instruction#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "ld " + this.register + " " + this.variable +" " + used;
	}

	/* (non-Javadoc)
	 * @see Instruction#getType()
	 */
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Load";
	}

}
