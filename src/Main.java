/**
 * Created by SoL on 29/10/2015.
 */

// import java.io.File;


import java.io.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Main {

	public static List<CFGraph> graphList;
	
    public static void main(String[] args)
    {
    	/*

        CFNode currentNode = new CFNode(00);  // may adjust this later on

        //List<CFNode> theFunction = new ArrayList<CFNode>(); // a function is just a list of Nodes/ Blocks
		//One graph for a function as defined on piazza. list of graph(functions) for a program.
        graphList = new ArrayList<CFGraph>();
        //new ArrayList<Instruction>();

        //File file = new File("/path/to/my/file.txt");
        //  /Users/SoL/3109compileroptimiser/readTest.txt

        //File file = new File("/Users/SoL/3109compileroptimiser/readTest.txt");

        // File file = new File("/Users/SoL/3109compileroptimiser/readTest.txt");
        try
        {
            System.out.println("What the file contained: ");

            // Open the file that is the first
            // command line parameter

            // using MAC here,  the directory  format might be different depneindg on OS,
            // also the libraries that are required to import


            // simpleMain.txt,  instructionsList.txt,  multiBlocks.txt
            FileInputStream fstream = new FileInputStream("multiBlocksUnreachable.txt");   //   simpleMain.txt


            // Get the object of DataInputStream
            DataInputStream inputStream = new DataInputStream(fstream);

            BufferedReader brReader = new BufferedReader(new InputStreamReader(inputStream));
            String strLine;

            //Read File Line By Line
            while ((strLine = brReader.readLine()) != null)
            {

                String editedString = editLine(strLine); // removing all brackets and  spacing front/ end

                //System.out.println(editedString);   // use this to see effect
                System.out.println(strLine);

                String[] splitEditedArray = editedString.split(" ");

                //if(splitEditedArray[0].equals("0"))
                //("[0-9].*")
                if(splitEditedArray[0].matches("[0-9].*")) // if the first one is a (block) number,   0, 19, 100 all work
                {

//                    System.out.println("You got a new block");
                    
                    //construct

                    int theId = Integer.parseInt(splitEditedArray[0]);
                    currentNode = new CFNode(theId);

                    //myNode.showContent();

                    editedString = "";

                    //  basically just remove the first element (  101 ld r1 n )  ----->  ld r1 n
                    for (int i = 1; i < splitEditedArray.length; i++)
                    {
                        editedString+= splitEditedArray[i] + " ";
                    }

                    editedString = editedString.trim();
                    splitEditedArray = editedString.split(" ");

                    //editedString = splitEditedArray.substring(1);
                }

                if(splitEditedArray[0].equals("ld")) //ld r3 tmp
                {
                    //System.out.println("You got an ld");

                    //String justTheInt = splitEditedArray[1].replaceAll("r", "");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    currentNode.addInstruction(new Load(theRegister, splitEditedArray[2]));

                }


                else if(splitEditedArray[0].equals("lc"))
                {
                    //System.out.println("You got a lc");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    int constant = Integer.parseInt(splitEditedArray[2]);

                    currentNode.addInstruction(new LoadConstant(theRegister, constant));

                }

                else if(splitEditedArray[0].equals("st")) //st tmp r2
                {
                    //System.out.println("You got a st " + splitEditedArray[1].toString());

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[2].replaceAll("r", ""));

                    currentNode.addInstruction(new StoreInst(theRegister, splitEditedArray[1].toString()));

                }

                // Arithmatic

                else if(isArithmetic(splitEditedArray[0])) // add r3 r4 r5
                {
                    //System.out.println("You got a Math");

                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    int registerOp1 = Integer.parseInt(splitEditedArray[2].replaceAll("r", ""));
                    int registerOp2 = Integer.parseInt(splitEditedArray[3].replaceAll("r", ""));

                    //public Arithmetic(String type, int regT, int regOp1, int regOp2){

                    currentNode.addInstruction(new Arithmetic(splitEditedArray[0], theRegister, registerOp1, registerOp2));
                }
                // Comparison

                else if(isComparison(splitEditedArray[0]))   // eq r3 r4 r5 ,   lt r1 r2 r4
                {
                    //System.out.println("You got a Comparison");

                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    int registerOp1 = Integer.parseInt(splitEditedArray[2].replaceAll("r", ""));
                    int registerOp2 = Integer.parseInt(splitEditedArray[3].replaceAll("r", ""));

                    // public Comparison(String type, int regT, int regOp1, int regOp2){

                    currentNode.addInstruction(new Comparison(splitEditedArray[0], theRegister, registerOp1, registerOp2));
                }

                else if(splitEditedArray[0].equals("call")) //st tmp r2
                {
                    //System.out.println("You got a call");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    List<Integer> registerList = new ArrayList<Integer>();

                    // private List<Integer> registerParameterList;
                    for(int i = 3; i < splitEditedArray.length; i++) // storing all registers input
                    {
                        int theInputRegister = Integer.parseInt(splitEditedArray[i].replaceAll("r", ""));

                        registerList.add(theInputRegister);
                    }

                    //CallInst(int registerReceiver, String functionName, List<Integer> registerParameterList)
                    currentNode.addInstruction(new CallInst(theRegister, splitEditedArray[2], registerList));

                }

                //current assume br / ret are the only two possible last instruction,
                else if(splitEditedArray[0].equals("ret")) //ret r3
                {
                    //System.out.println("You got a ret");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    currentNode.addInstruction(new ReturnInst(theRegister));

                    //current assume br / ret are the only two possible last instruction, then add into function
                    //??I doubt it
                    graphList.get(graphList.size()-1).add(currentNode);

                    //System.out.println("Block number: " + theFunction.size());

                }

                else if(splitEditedArray[0].equals("br"))
                {
                    //System.out.println("You got a branch");

                    // take the  rid ,  r14 ---> 14
                    int theRegister = Integer.parseInt(splitEditedArray[1].replaceAll("r", ""));

                    int leftBranch = Integer.parseInt(splitEditedArray[2]);
                    int rightBrnach = Integer.parseInt(splitEditedArray[3]);

                    currentNode.addInstruction(new Branch(theRegister, leftBranch, rightBrnach));

                    //current assume br / ret are the only two possible last instruction, then add into function
                    //??
                    graphList.get(graphList.size()-1).add(currentNode);

                    //System.out.println("Block number: " + theFunction.size());
                }
                else {
//                	System.out.println("You got a function!");
                	//construct a new graph here.
                	graphList.add(new CFGraph(editedString));
                }
            }
            //Close the input stream
            inputStream.close();
//            System.out.println("Number of blocks stored: " + theFunction.size());
            
            establishEdges();
            
            for (CFGraph g:graphList){
            	g.removeUnreachableNode();
            }
            
            System.out.println("Printing stored blocks: " );
            printProgram();
        }

        catch (Exception e){//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }


//        for (int i = 0; i < theFunction.size(); i++)
//        {
//            theFunction.get(i).showContent();
//
//        }

    // end of Main
        */
    }

    public static void printProgram(){
    	for (CFGraph g:graphList){
    		System.out.print('(' + g.toString() + ')');
    	}
    }
    
    
    
    private static String editLine(String uneditedLine)
    {
        //String editedLine

        String editedString = uneditedLine.replaceAll("\\(", ""); // removing all ( each line
        editedString = editedString.replaceAll("\\)", "");   // removing all )  each line

        editedString = editedString.trim();   // removing all spacing front / end

        return editedString;
    }

    private static boolean isArithmetic(String inputString)
    {
        //ArrayList<String> listOfArith = new ArrayList<String>();
        //listOfArith = ["add", "sub", "mul", "div"];

        String[] listOfArith = new String[] {"add","sub","mul","div"};

        if(Arrays.asList(listOfArith).contains(inputString))
            return true;
        //System.out.println("well you got " + inputString + " here");

        else
            return false;
    }


    private static boolean isComparison(String inputString)
    {

        String[] listOfComparison = new String[] {"lt","gt","eq"};

        if(Arrays.asList(listOfComparison).contains(inputString))
            return true;

        else
            return false;

    }
    
    private static void establishEdges(){
    	for (CFGraph singleGraph : graphList){
    		for (CFNode singleNode : singleGraph.getAllNodes()){
    			if (singleNode.getLastInstruction().getType().equals("Branch")){
    				Branch lastBranch = (Branch) singleNode.getLastInstruction();
    				
    				System.out.println(lastBranch.toString());
    				CFNode tempNode = singleGraph.getByID(lastBranch.getLeft());
    				if (tempNode!=null){
    					singleNode.setLeftChild(tempNode);
    				}
    				else{
    					System.out.println(lastBranch.toString() + "  didn't find left block" );
    				}
    				
    				tempNode = singleGraph.getByID(lastBranch.getRight());
    				if (tempNode!=null){
    					singleNode.setRightChild(tempNode);
    				}
    				else{
    					System.out.println(lastBranch.toString() + "  didn't find right block" );
    				}
    			}
    		}
    	}
    }
}
