/**
 * 
 */

/**
 * @author yuan
 *
 */
public class LoadConstant extends Instruction {

	private Register register;
	private int constant;
	
	public LoadConstant(Register register,int constant){
		this.register = register;
		this.constant = constant;
		this.used = false;
	}
	public Register getRegister(){
		return register;
	}
	/* (non-Javadoc)
	 * @see Instruction#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "lc " + this.register + " " + this.constant ;
	}

	/* (non-Javadoc)
	 * @see Instruction#getType()
	 */
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "LoadConstant";
	}

}
