/**
 * 
 */

/**
 * @author yuan
 *
 */
public class Branch extends Instruction {
	
	private Register register;
	private int leftBranch;
	private int rightBranch;
	
	public Branch(Register registerNo, int leftBranch, int rightBranch){
		this.register = registerNo;
		this.leftBranch = leftBranch;
		this.rightBranch = rightBranch;
		this.used = false;
	}
	/* (non-Javadoc)
	 * @see Instruction#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "br " + register + " " + leftBranch + " " +rightBranch +" " + used;
	}

	/* (non-Javadoc)
	 * @see Instruction#getType()
	 */
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Branch";
	}
	
	public Register getRegister(){
		return register;
	}
	
	public int getLeft(){
		return leftBranch;
	}
	
	public int getRight(){
		return rightBranch;
	}

}
