import java.util.List;

/**
 * 
 */

/**
 * @author yuan
 *
 */
public class CallInst extends Instruction {
	
	private Register registerReceiver;
	private String functionName;
	private List<Register> registerParameterList;
	
	public CallInst(Register registerReceiver, String functionName, List<Register> registerParameterList){
		this.registerReceiver =registerReceiver;
		this.functionName = functionName;
		this.registerParameterList = registerParameterList;
		this.used = false;
	}
	
	/* (non-Javadoc)
	 * @see Instruction#toString()
	 */
	@Override
	public String toString()
	{
		// TODO  Auto-generated method stub
		String toReturn = "call " + this.registerReceiver + " " + this.functionName;
		for (Register e:registerParameterList){
			toReturn += " ";
			toReturn += e;
		}
		return toReturn;
	}
	public List<Register> getRegisterOp(){
		return registerParameterList;
	}
	
	public Register getRegister(){
		return registerReceiver;
	}

	/* (non-Javadoc)
	 * @see Instruction#getType()
	 */
	@Override
	public String getType()
	{
		// TODO Auto-generated method stub
		return "CallInst";
	}

}
