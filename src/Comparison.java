/**
 * 
 */

/**
 * @author yuan
 *
 */
public class Comparison extends Instruction {
	
	private String type;
	private Register registerTarget;
	private Register registerOp1;
	private Register registerOp2;
	
	public Comparison(String type, Register regT, Register regOp1, Register regOp2){
		this.type = type;
		this.registerTarget = regT;
		this.registerOp1 = regOp1;
		this.registerOp2 = regOp2;
		this.used = false;
	}
	
	/* (non-Javadoc)
	 * @see Instruction#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return type + " " + registerTarget + " " + this.registerOp1 + " " + this.registerOp2;
	}

	/* (non-Javadoc)
	 * @see Instruction#getType()
	 */
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	public Register getRegister(){
		return registerTarget;
	}
	public Register getRegisterOp1(){
		return registerOp1;
	}
	public Register getRegisterOp2(){
		return registerOp2;
	}
}
